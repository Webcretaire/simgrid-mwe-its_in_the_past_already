# "It's in the past already" bug MWE

To reproduce, simply use 
```shell
cd build && \
    cmake .. && \
    make -j 2 && \
    ./current ../platform.xml ../deploy.xml --cfg=surf/precision:1e-9 
```

Or, if you trust me, an example output already exists in `out.txt` (error case)
