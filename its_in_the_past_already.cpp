#include <simgrid/s4u.hpp>
#include <string>
#include <vector>

using namespace std;

static void master(vector<string> args) {
  // De manière assez amusante, si tu enlèves la ligne suivante l'erreur disparait
  simgrid::s4u::this_actor::execute(42);

  auto mbx1 = simgrid::s4u::Mailbox::by_name("mailbox1");
  auto mbx2 = simgrid::s4u::Mailbox::by_name("mailbox2");

  auto comms = vector<simgrid::s4u::CommPtr>(2);
  void *ev_ptr = malloc(1);

  comms[0] = mbx1->get_async((void **)&ev_ptr);
  comms[1] = mbx2->get_async((void **)&ev_ptr);

  int which = simgrid::s4u::Comm::wait_any_for(&comms, 0);
}

int main(int argc, char *argv[]) {
  simgrid::s4u::Engine e(&argc, argv);

  e.register_function("master", &master);

  e.load_platform(argv[1]);
  e.load_deployment(argv[2]);

  e.run();

  return 0;
}
